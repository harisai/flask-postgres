### Install virtualenv and set python3 environment
    virtualenv venv --python=python3
  
### get into virtual env
    source venv/bin/activate
    
### Install requirements
    pip install -r requirements.txt
    
### Set environment variables
     export DATABASE_URL="postgresql://username:password@localhost:5432/db_name"
     
     (or)
     
     #if no username and password
     export DATABASE_URL="postgresql://localhost/db_name"
     
     #Setting dev environement
     export APP_SETTINGS='config.DevelopmentConfig'
     